package com.example.yourfoodss

import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity

class DrugaStorna : AppCompatActivity() {
    private lateinit var editTextFoodName: EditText
    private lateinit var buttonAddFood: Button
    private lateinit var listViewFood: ListView
    private lateinit var foodList: MutableList<String>
    private lateinit var adapter: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.druga_storna)

        editTextFoodName = findViewById(R.id.editTextFoodName)
        buttonAddFood = findViewById(R.id.buttonAddFood)
        listViewFood = findViewById(R.id.listViewFood)

        foodList = mutableListOf()
        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, foodList)
        listViewFood.adapter = adapter

        buttonAddFood.setOnClickListener {
            val foodName = editTextFoodName.text.toString()
            if (foodName.isNotEmpty()) {
                foodList.add(foodName)
                adapter.notifyDataSetChanged()
                editTextFoodName.text.clear()
            }
        }
    }
}
