package com.example.yourfoodss

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.yourfoodss.R

class MainActivity : AppCompatActivity() {
    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var buttonLogin: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)

        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        buttonLogin = findViewById(R.id.buttonLogin)

        buttonLogin.setOnClickListener {
            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()

            if (email.isNotEmpty() && password.isNotEmpty()) {
                // Tutaj dodaj logikę logowania, np. sprawdzanie poprawności danych

                // Przykład sprawdzenia, czy hasło to "password"
                if (password == "password") {
                    Toast.makeText(this, "Logowanie udane!", Toast.LENGTH_SHORT).show()
                    // Przekieruj na kolejny ekran po zalogowaniu
                } else {
                    Toast.makeText(this, "Nieprawidłowy email lub hasło", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, "Wprowadź email i hasło", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
